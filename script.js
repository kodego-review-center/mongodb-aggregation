db.fruits.insertMany (
    [
        {
            name: Apple,
            color: "Red",
            stock: 20,
            price: 40,
            supplier_id: 1,
            onSale: true,
            origin: ["Philippines", "US"]
        },

        {
            name: "Banana",
            color: "Yellow",
            stock: 15,
            price: 20,
            supplier_id: 2,
            onSale: true,
            origin: ["Philippines", "Ecuador"]
        },

        {
            name: "Kiwi",
            color: "Green",
            stock: 25,
            price: 50,
            supplier_id: 1,
            onSale: true,
            origin: ["US", "China"]
        },

        {
            name: "Mango",
            color: "Yellow",
            stock: 10,
            price: 120,
            supplier_id: 2,
            onSale: false,
            origin: ["Philippines", "India"]
        }
    ]
);

//first stage: search for fruits documents that is currently on sale

db.fruits.aggregate(
    [
        {$match: {onsale:true}}
    ]
);

//second stage: group these documents according to the supplier_id and get the total value of stock

db.fruits.aggregate
(
	[
		{$group: 
			{ _id: "$supplier_id", 
			total: { $sum: "$stock"} }}
	]
);

//third stage: exclude _id field

db.fruits.aggregate
(
	[
		{$group: 
			{ _id: "$supplier_id", 
			total: { $sum: "$stock"} }
        },
			{$project: {_id:0}}
	]
);

//fourth stage: sort the total in an ascending order
db.fruits.aggregate
(
	[
		{$group: 
			{ _id: "$supplier_id", 
			total: { $sum: "$stock"} }
        },
			{$project: {_id:0}},
            ($sort: {total:1})
	]
);

